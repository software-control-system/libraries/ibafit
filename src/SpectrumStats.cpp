//*****************************************************************************
//* Copyright 2010 - 2013, Thales Optronique SAS - All rights reserved        *
//*****************************************************************************
//
//=============================================================================
// file :        SpectrumStats.cpp
//
// description : This file contains the implementation of the Spectrum statistics tools.
//
// project :     StatisticsSpectrum
//
// $Author:  THALES OPTRONIQUE SAS
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//=============================================================================
#include "SpectrumStats.h"

#include <vector>
#include <algorithm>

namespace SpectrumStats
{
	void calculateIntegral(const double* ai_spectrum, int ai_spectrumSize, double& ao_integral)
	{
		double w_integral = 0;
		const double* w_tail = ai_spectrum + ai_spectrumSize;
		for (const double* w_iterator = ai_spectrum; w_iterator < w_tail; ++w_iterator)
		{
			w_integral += *w_iterator;
		}
		ao_integral = w_integral;
	}

	void calculateBarycenter(const double* ai_spectrum, 
							int ai_spectrumSize, 
							double ai_integral, 
							double& ao_barycenter, 
							unsigned long& ao_barycenterId)
	{
		unsigned long w_pos = 0;
		const double w_barycenter = ai_integral / 2;

		if (ai_spectrumSize > 0)
		{
			const double* w_tail = ai_spectrum + ai_spectrumSize;

			double w_acc = *ai_spectrum;
			
			for (const double* w_iterator = ai_spectrum + 1; (w_acc < w_barycenter) && (w_iterator < w_tail); ++w_iterator)
			{
				w_acc += *w_iterator;
				++w_pos;
			}
		}
		
		ao_barycenter = w_barycenter;
		ao_barycenterId = w_pos;
	}

	void calculateIntegralBarycenter(const double* ai_spectrum, 
									int ai_spectrumSize, 
									double& ao_integral, 
									double& ao_barycenter, 
									unsigned long& ao_barycenterId)
	{
		calculateIntegral(ai_spectrum, ai_spectrumSize, ao_integral);
		calculateBarycenter(ai_spectrum, ai_spectrumSize, ao_integral, ao_barycenter, ao_barycenterId);
	}


	CurveWidth calculateWidthAtHigh(const double* ai_spectrum,
									size_t ai_spectrumSize,
									double ai_high)
	{
		CurveWidth w_res(ai_high);

		if (ai_spectrumSize == 0)
			return w_res;

		// Looking for the spectrum maximum
		double w_max = *std::max_element(ai_spectrum, ai_spectrum + ai_spectrumSize);
		
		const double w_threshold = w_max * ai_high;

		std::vector<size_t> w_risingEdge;
		std::vector<size_t> w_fallingEdge;

		for (size_t i = 0; i < ai_spectrumSize - 1; ++i)
		{
			if ((ai_spectrum[i] <= w_threshold) && (w_threshold < ai_spectrum[i + 1]))
			{
				w_risingEdge.push_back(i);
			}
			else if ((ai_spectrum[i] >= w_threshold) && (w_threshold > ai_spectrum[i + 1]))
			{
				w_fallingEdge.push_back(i);
			}
		}

		w_res.m_posLow = (w_risingEdge.size() == 0) ? 0 : w_risingEdge.front();
		w_res.m_posHigh = (w_fallingEdge.size() == 0) ? ai_spectrumSize : w_fallingEdge.back();
		w_res.m_width = w_res.m_posHigh - w_res.m_posLow;

		if (w_res.m_posLow <= w_res.m_posHigh)
			w_res.setValid(true);
		
		return w_res;
	}

	CurveWidth calculateFWHM(const double* ai_spectrum,
							size_t ai_spectrumSize)
	{
		return calculateWidthAtHigh(ai_spectrum, ai_spectrumSize, 0.5);
	}

	CurveWidth calculate1_e2(const double* ai_spectrum,
							size_t ai_spectrumSize)
	{
		return calculateWidthAtHigh(ai_spectrum, ai_spectrumSize, MATHCONST_1_E2);
	}


	void reduceSpectrum(int ai_input_spectrumSize,const double* ai_input_spectrum,int ai_output_spectrumSize,double* ao_output_spectrum)
	{
		if(ai_input_spectrumSize==0)
		{
			return;
		}
		if(ai_output_spectrumSize==0)
		{
			return;
		}
		const double step=(((double)ai_input_spectrumSize)/((double)ai_output_spectrumSize));
		const double halfStep=(step/2.0);

		int j=0;//index for out spectrum
		int randomValue=0;//index to add

		//The spectrum is reduced using a classic undersampling by random selection algorithm.
		//Each point in the out spectrum is a random selection among the N points of the input spectrum inside the "step"
		//Example:
		//For an input spectrum size of 4000, an output size of 100, step size is 40 (4000/100)
		//First point of output is selected among point from 0 to 19
		//Second point of output is selected among point from point 20 to 59 ...
		//...
		//Last point of output is selected among point from 3980 to 4000.

		//ao_output_spectrum[0]=ai_input_spectrum[0];//First copie of first element

		for(int i=0; i<ai_output_spectrumSize; i++)//Iterate over each input element
		{
			if(i==0)
			{
				//First case
				if(halfStep>=1)
				{
				randomValue=(rand()%((int)halfStep));
				}
				else
				{
				randomValue=1;
				}
			}
			else if (i>(ai_input_spectrumSize-halfStep))
			{
				//Last case
				if(halfStep>=1)
				{
				randomValue=(rand()%((int)halfStep));
				}
				else
				{
				randomValue=1;
				}
			}
			else
			{
				//other cases
				if(step>=1)
				{
				randomValue=(rand()%((int)step));
				}
				else
				{
				randomValue=1;
				}
			}
			//randomValue=1;
			ao_output_spectrum[i]=ai_input_spectrum[(int)(i*step)+randomValue];
		}
	}

	//Same method, see comment in method above
	void reduceSpectrum(int ai_input_spectrumSize,const float* ai_input_spectrum,int ai_output_spectrumSize,float* ao_output_spectrum)
	{
		const float step=(((float)ai_input_spectrumSize)/((float)ai_output_spectrumSize));
		const float halfStep=(step/2.0);

		int j=0;//index for out spectrum
		int randomValue=0;//index to add

		int weight=1;//ponderation		
		ao_output_spectrum[0]=ai_input_spectrum[0];//First copie of first element

		for(int i=0; i<ai_output_spectrumSize; i++)//Iterate over each input element
		{
			if(i==0)
			{
				//First case
				if(halfStep>=1)
				{
				randomValue=(rand()%((int)halfStep));
				}
				else
				{
				randomValue=1;
				}
			}
			else if (i>(ai_input_spectrumSize-halfStep))
			{
				//Last case
				if(halfStep>=1)
				{
				randomValue=(rand()%((int)halfStep));
				}
				else
				{
				randomValue=1;
				}
			}
			else
			{
				//other cases
				if(step>=1)
				{
				randomValue=(rand()%((int)step));
				}
				else
				{
				randomValue=1;
				}
			}
			//randomValue=1;
			ao_output_spectrum[i]=ai_input_spectrum[(int)(i*step)+randomValue];
		}
	}

}
