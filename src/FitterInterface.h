//*****************************************************************************
//* Copyright 2010 - 2013, Thales Optronique SAS - All rights reserved        *
//*****************************************************************************
//
//=============================================================================
// file :        FitterInterface.h
//
// description : This file defines the FitterInterface abstract class.
//
// project :     Fitter
//
// $Author:  THALES OPTRONIQUE SAS
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//=============================================================================
#ifndef FitterInterface_H
#define FitterInterface_H

#include <string>

namespace Fitter_ns
{
	/**
	 * This class defines a generic interface for any specific function fitting class.
	*/
	class FitterInterface
	{
	public:
		/**
		 * The apply method shall launch the fitting on provided X and Y data sets. Abstract method.
		 * param[in] ai_dataSize Data sets size.
		 * param[in] ai_XData Abscissa data.
		 * param[in] ai_YData Ordinate data.
		*/
		virtual int apply(size_t ai_dataSize, const double* ai_XData, const double* ai_YData) = 0;

		/**
		 * The apply method shall launch the fitting on provided Y data sets. Abstract method.
		 * param[in] ai_dataSize Data sets size.
		 * param[in] ai_YData Ordinate data.
		*/
		virtual int apply(size_t ai_dataSize, const double* ai_YData) = 0;

		/**
		 * The dumpCalculatedValues method shall dump in a file all calculated scalar data in a 
		 * CSV compatible format. Abstract method.
		 * param[in] filename Output data filename.
		*/
		virtual void dumpCalculatedValues(const std::string& filename) = 0;

		/**
		 * The dumpVectorData method shall dump in a file all calculated spectrum data in a CSV 
		 * compatible format. Abstract method.
		 * param[in] filename Output data filename.
		*/
		virtual void dumpVectorData(const std::string& filename) = 0;

		/**
		 * The setIterationNumber method shall set the maximum iteration number which the fitting
		 * algorithm may do. Abstract method.
		 * param[in] filename Output data filename.
		*/
		virtual void setIterationNumber (int ai_nbIteration) = 0;
	};
}
#endif /* !FitterInterface_H */