//*****************************************************************************
//* Copyright 2010 - 2013, Thales Optronique SAS - All rights reserved        *
//*****************************************************************************
//
//=============================================================================
// file :        Fitter.cpp
//
// description : This file contains the implementation of the Fitter class methods.
//
// project :     Fitter
//
// $Author:  THALES OPTRONIQUE SAS
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//=============================================================================
#include "Fitter.h"

#include <vector>
#include "string.h"

namespace Fitter_ns
{
	Fitter::Fitter(void)
		: m_iterationNbr(0)
		, m_epsilon(0)
		, m_hasConverged(false)		
		, m_control(lm_control_double)
		, m_xData(0)
		, m_yData(0)
		, m_fittedData(0)
		, m_residualData(0)
		, m_dataSize(0)
		, m_status(0)
	{
	}

	Fitter::~Fitter(void)
	{
		cleanAll();
	}

	int Fitter::apply(size_t					ai_paramNbr,
					double*						aio_param, 
					size_t						ai_dataNbr,
					const double*				ai_dataY,
					double (*function)(double, const double*))
	{
		std::vector<double> w_dataX;
		w_dataX.reserve(ai_dataNbr);
		for (size_t i = 0; i < ai_dataNbr; ++i)
			w_dataX.push_back(i);

		return apply(ai_paramNbr, aio_param, ai_dataNbr, &(*(w_dataX.begin())), ai_dataY, function);
	}

	int Fitter::apply(size_t					ai_paramNbr,
					double*						aio_param, 
					size_t						ai_dataNbr,
					const double*				ai_dataX,
					const double*				ai_dataY,
					double (*function)(double, const double*))
	{
		cleanAll();

		lm_status_struct status;

		double orderGuessed = aio_param[2];

		lmcurve(static_cast<int>(ai_paramNbr), aio_param, static_cast<int>(ai_dataNbr), ai_dataX, ai_dataY, function, &m_control, &status );

		if (aio_param[2] < orderGuessed)
		{
			aio_param[2] = orderGuessed;
		}

		m_iterationNbr = status.nfev;
		m_epsilon = status.fnorm;
		if ((status.outcome >= 1) && (status.outcome <= 3))
			m_hasConverged = true;
//		m_status = _strdup(lm_infmsg[status.outcome]);
		m_status = strdup(lm_infmsg[status.outcome]);

		m_dataSize = ai_dataNbr;
		m_fittedData = new double[ai_dataNbr];
		m_residualData = new double[ai_dataNbr];
		m_xData = new double[ai_dataNbr];
		m_yData = new double[ai_dataNbr];

		for (size_t i = 0; i < ai_dataNbr; ++i)
		{
			m_xData[i] = ai_dataX[i];
			m_yData[i] = ai_dataY[i];

			const double w_val = function(ai_dataX[i], aio_param);
			m_fittedData[i] = w_val;
			m_residualData[i] = ai_dataY[i] - w_val; 
		}

		return 0;
	}

	void Fitter::cleanAll()
	{
		if (m_xData)
		{
			delete[] m_xData;
			m_xData = 0;
		}
		if (m_yData)
		{
			delete[] m_yData;
			m_yData = 0;
		}
		if (m_fittedData)
		{
			delete[] m_fittedData;
			m_fittedData = 0;
		}
		if (m_residualData)
		{
			delete[] m_residualData;
			m_residualData = 0;
		}
		m_dataSize = 0;

		m_iterationNbr = 0;
		m_epsilon = 0;
		m_hasConverged = false;

		if (m_status)
		{
			delete[] m_status;
			m_status =0;
		}
	}
}