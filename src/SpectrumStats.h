//*****************************************************************************
//* Copyright 2010 - 2013, Thales Optronique SAS - All rights reserved        *
//*****************************************************************************
//
//=============================================================================
// file :        SpectrumStats.h
//
// description : This file defines the Spectrum statistics tools.
//
// project :     StatisticsSpectrum
//
// $Author:  THALES OPTRONIQUE SAS
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//=============================================================================
#pragma once

#include <cmath>
#include <string.h>

namespace SpectrumStats
{
	const double MATHCONST_E = 2.71828182845904523536;
	const double MATHCONST_1_E2 = 1 / pow(MATHCONST_E, 2);


	/**
	 * This structure is used as return value for calculateWidthAtHigh() and calculateFWHM() methods.
	 * It contains calculated data + a validity flag.
	*/
	typedef struct CurveWidth
	{
	public:
		/**
		 * CurveWidth default Ctor
		*/
		CurveWidth(double highOfWidth)
			: m_highOfWidth(highOfWidth)
			, m_posLow(0)
			, m_posHigh(0)
			, m_width(0)
			, m_valid(false)
		{}

		/**
		 * CurveWidth copy Ctor
		*/
		CurveWidth(const CurveWidth& src)
		{
			*this = src;
		}

		/**
		 * Definition of the operator =
		*/
		const CurveWidth & operator= (const CurveWidth & src) 
		{
			if (&src == this)
				return *this;

			this->m_highOfWidth = src.m_highOfWidth;
			this->m_posLow = src.m_posLow;
			this->m_posHigh = src.m_posHigh;
			this->m_width = src.m_width;
			this->setValid(src.isValid());
			return *this;
		}
		/**
		 * Set the validity flag value.
		*/
		void setValid(bool v)
		{
			m_valid = v;
		}

		/**
		 * Acquire the validity flag value.
		 *
		 * @return Returns a boolean value representing the validity state of this structure.
		*/
		bool isValid() const
		{
			return m_valid;
		}

	public:	
		/**
		 * m_highOfWidth:	Percentage of the maximum of the curve at which the width shall be calculated. The value of the threshold is : threshold = m_highOfWidth * curveMaximum.
		 */
		double	m_highOfWidth;
		/**
		 * m_posLow:	First curve id where (curve[i] < threshold < curve[i+1]) or (curve[i] > threshold > curve[i+1]).
		 */
		size_t	m_posLow;
		/**
		 * m_posHigh:	Last curve id where (curve[i] < threshold < curve[i+1]) or (curve[i] > threshold > curve[i+1]).
		 */
		size_t	m_posHigh;
		/**
		 * m_width:	Width of the curve at given percentage of the maximum. m_width = m_posHigh - m_posLow;
		 */
		double	m_width;

	private:
		bool	m_valid;

	} CurveWidth;

	/**
	 * The calculateIntegral() method computes the integral of a given spectrum. The integral of a spectrum is 
	 * equivalent to the cumulated sum of all points of the spectrum.
	 *
 	 *	@param argin ai_spectrum: Target spectrum of which barycenter and barycenterId shall be calculated.
	 *	@param argin ai_spectrumSize: Spectrum number of points.
 	 *	@param argin ao_integral: Outut integral value.
	 */ 
	void calculateIntegral(const double* ai_spectrum, int ai_spectrumSize, double& ao_integral);

	/**
	 * The calculateBarycenter() method computes the barycenter and its position for a given spectrum with a given integral.
	 *
 	 *	@param argin ai_spectrum: Target spectrum of which barycenter and barycenterId shall be calculated.
	 *	@param argin ai_spectrumSize: Spectrum number of points.
 	 *	@param argin ai_integral: Input integral value.
	 *	@param argin ao_barycenter: Output barycenter value.
 	 *	@param argin ao_barycenterId: Output index value. It matches the position of the barycenter in the Spectrum.
	 */ 
	void calculateBarycenter(const double* ai_spectrum, 
							int ai_spectrumSize, 
							double ai_integral, 
							double& ao_barycenter, 
							unsigned long& ao_barycenterId);


	/**
	 * The calculateIntegralBarycenter() method computes integral and the barycenter of a given spectrum.
	 *
 	 *	@param argin ai_spectrum: Target spectrum of which the integral and barycenter shall be calculated.
	 *	@param argin ai_spectrumSize: Spectrum number of points.
 	 *	@param argin ao_integral: Output integral value.
	 *	@param argin ao_barycenter: Output barycenter value.
 	 *	@param argin ao_barycenterId: Output index value. It matches the position of the barycenter in the Spectrum.
	 */ 
	void calculateIntegralBarycenter(const double* ai_spectrum, 
									int ai_spectrumSize, 
									double& ao_integral, 
									double& ao_barycenter, 
									unsigned long& ao_barycenterId);


	/**
	 * The calculateWidthAtHigh() method computes the width of the spectrum at a given percent of the maximum high.
	 *
 	 *	@param argin ai_spectrum: Target spectrum of which width shall be calculated.
	 *	@param argin ai_spectrumSize: Spectrum number of points.
 	 *	@param argin ai_high: Percent of the maximum high of the spectrum at which the width shall be calculated. (ai_high = 1. = 100%).
	 *
	 *  @return Returns a CurveWidth structure which contains all calculated data. The flag "isValid" is set to false if a error appened.
	 */ 
	CurveWidth calculateWidthAtHigh(const double* ai_spectrum,
									size_t ai_spectrumSize,
									double ai_high);

	/**
	 * The calculateFWHM() method computes the width of the spectrum at half maximum. This method calls calculateWidthAtHigh() with "ai_high" argument set to 0.5
	 *
 	 *	@param argin ai_spectrum: Target spectrum of which width shall be calculated.
	 *	@param argin ai_spectrumSize: Spectrum number of points.
	 *
	 *  @return Returns a CurveWidth structure which contains all calculated data. The flag "isValid" is set to false if a error appened.
	 */ 
	CurveWidth calculateFWHM(const double* ai_spectrum,
							size_t ai_spectrumSize);

		/**
	 * The calculate1_e2() method computes the width of the spectrum at 1/e2 of the maximum. This method calls calculateWidthAtHigh() with "ai_high" argument set to 1/e2
	 *
 	 *	@param argin ai_spectrum: Target spectrum of which width shall be calculated.
	 *	@param argin ai_spectrumSize: Spectrum number of points.
	 *
	 *  @return Returns a CurveWidth structure which contains all calculated data. The flag "isValid" is set to false if a error appened.
	 */ 
	CurveWidth calculate1_e2(const double* ai_spectrum,
							size_t ai_spectrumSize);

		/**
	 * The reduceSpectrum() method create a spectrum reduced
	 *
 	 *	@param argin ai_input_spectrumSize: Size of the input spectrum
	 *	@param argin ai_input_spectrum: The input spectrum
	 *	@param argin ai_output_spectrumSize: Number of point desired for output spectrums
	 *	@param argout ao_output_spectrum: Outputed spectrum
	 */ 
	void reduceSpectrum(int ai_input_spectrumSize,
						const double* ai_input_spectrum,
						int ai_output_spectrumSize,
						double* ao_output_spectrum						
							);

	/**
	 * The reduceSpectrum() method create a spectrum reduced
	 *
 	 *	@param argin ai_input_spectrumSize: Size of the input spectrum
	 *	@param argin ai_input_spectrum: The input spectrum
	 *	@param argin ai_output_spectrumSize: Number of point desired for output spectrums
	 *	@param argout ao_output_spectrum: Outputed spectrum
	 */ 
	void reduceSpectrum(int ai_input_spectrumSize,const float* ai_input_spectrum,int ai_output_spectrumSize,float* ao_output_spectrum);
}