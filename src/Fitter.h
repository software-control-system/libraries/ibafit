//*****************************************************************************
//* Copyright 2010 - 2013, Thales Optronique SAS - All rights reserved        *
//*****************************************************************************
//
//=============================================================================
// file :        Fitter.h
//
// description : This file defines the Fitter class.
//
// project :     Fitter
//
// $Author:  THALES OPTRONIQUE SAS
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//=============================================================================

#ifndef Fitter_H
#define Fitter_H

extern "C"
{
#include <lmcurve.h>
}

#if defined(WIN32) 
# if defined(FITTER_DLL)
#   define FITTER_DECL_EXPORT __declspec(dllexport)
#   define FITTER_DECL_IMPORT __declspec(dllimport)
#   if defined (FITTER_BUILD)
#     define FITTER_DECL FITTER_DECL_EXPORT
#   else
#     define FITTER_DECL FITTER_DECL_IMPORT
#   endif
# else
#   define FITTER_DECL_EXPORT
#   define FITTER_DECL_IMPORT
#   define FITTER_DECL
# endif
#else
#   define FITTER_DECL_EXPORT
#   define FITTER_DECL_IMPORT
#   define FITTER_DECL
#endif

namespace Fitter_ns
{
	/**
	 * This class is used as wrapper for the lmfit open source library.
	*/
	class FITTER_DECL Fitter
	{
	public:
		/**
		 * Fitter default Ctor
		*/
		Fitter(void);
		/**
		 * Fitter default Dtor
		*/
		~Fitter(void);

		/**
		 * setPatience method is linked to the maximal number of iterations of the algorithm. Inline method.
		 * @param[in] ai_nbPatience Number of the iteration by degree of freedom of the function : MaxIterationNumber = ai_nbPatience * (FreedomDegree + 1)
		*/
		inline void setPatience( int ai_nbPatience);
		/**
		 * setEpsilon method define the epsilon at which the algorithm will stop. Inline method.
		 * @param[in] ai_epsilon Epsilon value
		*/
		inline void setEpsilon( double ai_epsilon);

		/**
		 * getXData method returns the abscissa values given as initial data. These data were locally copied. Inline method. 
		 * @return The abscissa data. The array size is given by getDataSize() method.
		*/
		inline const double*	getXData() const;
		/**
		 * getYData method returns the ordinate values given as initial data. These data were locally copied. Inline method.
		 * @return The ordinate data. The array size is given by getDataSize() method.
		*/		
		inline const double*	getYData() const;
		/**
		 * getFittedData method returns the fitted spectrum data. Inline method.
		 * @return The fitted data. The array size is given by getDataSize() method.
		*/			
		inline const double*	getFittedData() const;
		/**
		 * getResidualData method returns the residual spectrum data. Inline method.
		 * @return The residual data. The array size is given by getDataSize() method.
		*/			
		inline const double*	getResidualData() const;
		/**
		 * getDataSize method returns the size of the XData, YData, FittedData and ResidualData spectrums. Inline method.
		 * @return Data array size.
		*/	
		inline const size_t		getDataSize() const;

		/**
		 * getIterationNumber method returns the number of iteration done by the fitting algorithm. Inline method.
		 * @return Iteration number.
		*/	
		inline int getIterationNumber() const;
		/**
		 * getEpsilon method returns final epsilon after the fitting finished. Inline method.
		 * @return Final epsilon.
		*/	
		inline double getEpsilon() const;
		/**
		 * hasConverged method allows the user to know if the fitter has converged. Inline method.
		 * @return Boolean value.
		*/	
		inline bool hasConverged() const;
		/**
		 * getStatus method returns final status after processing. For instance, here it is possible to know why 
		 * the fitter didnot converged Inline method.
		 * @return C type string.
		*/	
		inline const char* getStatus() const;

	private:
		void cleanAll();

	protected:
		/**
		 * The apply method wrappes the call to the fitting function of the LMFit library.
		 * param[in] ai_paramNbr Number of parameters.
		 * param[in] aio_param Parameters array.
		 * param[in] ai_dataNbr Data sets size.
		 * param[in] ai_dataX Abscissa data.
		 * param[in] ai_dataY Ordinate data.
		*/
		int apply(size_t				ai_paramNbr,
			double*						aio_param, 
			size_t						ai_dataNbr,
			const double*				ai_dataX,
			const double*				ai_dataY,
			double (*function)(double x, const double* pram));

		/**
		 * The apply method wrappes the call to the fitting function of the LMFit library.
		 * param[in] ai_paramNbr Number of parameters.
		 * param[in] aio_param Parameters array.
		 * param[in] ai_dataNbr Data sets size.
		 * param[in] ai_dataY Ordinate data.
		*/
		int apply(size_t				ai_paramNbr,
			double*						aio_param, 
			size_t						ai_dataNbr,
			const double*				ai_dataY,
			double (*function)(double x, const double* pram));

	private:

		int							m_iterationNbr;
		double						m_epsilon;
		bool						m_hasConverged;
		char*						m_status;
		lm_control_struct			m_control;

	protected:
		/**
		* m_xData : Initial abscissa data
		*/
		double*			m_xData;
		/**
		* m_yData : Initial ordinate data
		*/
		double*			m_yData;
		/**
		* m_fittedData : Final fitted spectrum
		*/
		double*			m_fittedData;
		/**
		* m_residualData : Final spectrum of residuals
		*/
		double*			m_residualData;
		/**
		* m_dataSize : Local spectrums size.
		*/
		size_t			m_dataSize;
	};

	const double*	Fitter::getXData() const
	{
		return m_xData;
	}
	
	const double*	Fitter::getYData() const
	{
		return m_yData;
	}
	const double*	Fitter::getFittedData() const
	{
		return m_fittedData;
	}

	const double*	Fitter::getResidualData() const
	{
		return m_residualData;
	}
	
	const size_t	Fitter::getDataSize() const
	{
		return m_dataSize;
	}

	int Fitter::getIterationNumber() const
	{
		return m_iterationNbr;
	}

	double Fitter::getEpsilon() const
	{
		return m_epsilon;
	}

	bool Fitter::hasConverged() const
	{
		return m_hasConverged;
	}

	const char* Fitter::getStatus() const
	{
		return m_status;
	}

	void Fitter::setPatience( int ai_nbPatience)
	{
		m_control.patience = ai_nbPatience;
	}

	void Fitter::setEpsilon( double ai_epsilon)
	{
		m_control.epsilon = ai_epsilon;
		m_control.xtol =  ai_epsilon;
		m_control.gtol =  ai_epsilon;
	}

}

#endif /* !Fitter_H */