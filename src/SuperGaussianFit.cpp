//*****************************************************************************
//* Copyright 2010 - 2013, Thales Optronique SAS - All rights reserved        *
//*****************************************************************************
//
//=============================================================================
// file :        SuperGaussianFit.cpp
//
// description : This file contains the implementation of the SuperGaussianFit class methods.
//
// project :     Fitter
//
// $Author:  THALES OPTRONIQUE SAS
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//=============================================================================
#include "SuperGaussianFit.h"
#include "SpectrumStats.h"

namespace Fitter_ns
{
	double SuperGaussianFunction (double x, const double* pram)
	{
		return pram[0] * exp( -1 * pow(fabs((x - pram[3]) / pram[1]), pram[2]));
	}


	SuperGaussianFit::SuperGaussianFit(void)
		: m_paramNbr(SUPERGAUSSIAN_NB_PARAMETERS)
	{
		m_paramVal = new double[m_paramNbr];
		memset(m_paramVal, 0, m_paramNbr * sizeof(double));
	}

	SuperGaussianFit::~SuperGaussianFit(void)
	{
		delete[] m_paramVal;
	}

	void SuperGaussianFit::setGuessParams(double ai_magnitude, double ai_width, double ai_order, double ai_center)
	{
		m_paramVal[0] = ai_magnitude;
		m_paramVal[1] = ai_width;
		m_paramVal[2] = ai_order;
		m_paramVal[3] = ai_center;
	}

	int SuperGaussianFit::apply(size_t ai_dataSize, const double* ai_XData, const double* ai_YData)
	{
		if (ai_dataSize <= 0)
			return -1;

		if (ai_XData == 0)
			return -2;

		if (ai_YData == 0)
			return -3;

		return Fitter::apply(m_paramNbr, m_paramVal, ai_dataSize, ai_XData, ai_YData, SuperGaussianFunction);
	}

	int SuperGaussianFit::apply(size_t ai_dataSize, const double* ai_YData)
	{
		if (ai_dataSize <= 0)
			return -1;

		if (ai_YData == 0)
			return -3;

		return Fitter::apply(m_paramNbr, m_paramVal, ai_dataSize, ai_YData, SuperGaussianFunction);
	}

	void SuperGaussianFit::setIterationNumber( int ai_nbIteration)
	{
		Fitter::setPatience( ai_nbIteration / (SUPERGAUSSIAN_NB_PARAMETERS + 1));
	}

	double SuperGaussianFit::getSigma() const
	{
		return sqrt(getVariance()); 
	}
		
	double SuperGaussianFit::getVariance() const
	{
		const double w_width = getWidth();
		return w_width / 2.; 
	}

	double SuperGaussianFit::getFWHM() const
	{
		//return getSigma() * FWHM_SCALING_FACTOR;

		SpectrumStats::CurveWidth w_fwhm = SpectrumStats::calculateFWHM(getFittedData(), getDataSize());
		if (! w_fwhm.isValid())
		{
			return -1;
		}

		return w_fwhm.m_width;
	}

	double SuperGaussianFit::get1_e2() const
	{
		SpectrumStats::CurveWidth w_1_e2 = SpectrumStats::calculate1_e2(getFittedData(), getDataSize());
		if (! w_1_e2.isValid())
		{
			return -1;
		}

		return w_1_e2.m_width;
	}
	double SuperGaussianFit::getChi2() const
	{
		double w_acc = 0.;
		for (size_t i = 0; i < getDataSize(); ++i)
			w_acc += (m_residualData[i] * m_residualData[i]);

		return w_acc;
	}


	void SuperGaussianFit::dumpCalculatedValues(const std::string& filename)
	{
		std::ofstream ofs(filename.c_str());

		ofs << "Magnitude\t" << getMagnitude() << std::endl;
		ofs << "Width\t" << getWidth() << std::endl;
		ofs << "Order\t" << getOrder() << std::endl;
		ofs << "Center\t" << getCenter() << std::endl;
		ofs << std::endl;
		ofs << "sigma\t" << getSigma() << std::endl;
		ofs << "variance\t" << getVariance() << std::endl;
		ofs << "FWHM\t" << getFWHM() << std::endl;
		ofs << "Chi2\t" << getChi2() << std::endl;
		ofs << std::endl;
		ofs << "IterNumber\t" << getIterationNumber() << std::endl;
		ofs << "epsilon\t" << getEpsilon() << std::endl;
		ofs << "hasConverged\t" << hasConverged() << std::endl;
		ofs << "Status\t[" << getStatus() << "]" << std::endl;

		ofs.close();
	}

	void SuperGaussianFit::dumpVectorData(const std::string& filename)
	{
		std::ofstream ofs(filename.c_str());

		ofs << "xData\tyData\tfittedData\tresidualData" << std::endl;

		for (size_t i = 0; i < getDataSize(); ++i)
		{
			ofs << getXData()[i] << "\t" << getYData()[i] << "\t" << getFittedData()[i] << "\t" << getResidualData()[i] << std::endl;
		}

		ofs.close();
	}

}