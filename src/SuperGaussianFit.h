//*****************************************************************************
//* Copyright 2010 - 2013, Thales Optronique SAS - All rights reserved        *
//*****************************************************************************
//
//=============================================================================
// file :        SuperGaussianFit.h
//
// description : This file defines the SuperGaussianFit class.
//
// project :     Fitter
//
// $Author:  THALES OPTRONIQUE SAS
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//=============================================================================
#ifndef SuperGaussianFit_H
#define SuperGaussianFit_H


#include "FitterInterface.h"
#include "Fitter.h"

#include <fstream>
#include <cmath>

/**
 * This namespace encapsulate all classes and tools linked to the Fitter library
 */
namespace Fitter_ns
{
	/**
	 * This factor is used to calculate the FWHM of the distribution
	 */
	const double FWHM_SCALING_FACTOR = sqrt(8 * log(2.f));
	/**
	 * This value defines the number of degrees of freedom of the super Gaussian fitting function.
	 */
	const unsigned short SUPERGAUSSIAN_NB_PARAMETERS = 4;

	/**
	* The C style function which defines the mathematical super Gaussian function to fit. 
	*  @param[in] x Number of parameters = 4
	*  @param[in] parameter Parameters of the super gaussian function.
	*			parameter[0] = magnitude = The guess shall be smaller or equal to the max value of the spectrum.
	*			parameter[1] = width = The guess should be smaller or equal to FWHM.
	*			parameter[2] = order = A good initial value is 2.
	*			parameter[3] = center = A good guess is the barycenter of the spectrum.
	*/
	static double SuperGaussianFunction (double x, const double* parameter);

	

	class FITTER_DECL SuperGaussianFit : public Fitter, public virtual FitterInterface
	{
	public:
		/**
		 * SuperGaussianFit default Ctor
		*/
		SuperGaussianFit(void);
		/**
		 * SuperGaussianFit default Dtor
		*/
		virtual	~SuperGaussianFit(void);

		/**
		 * The setGuessParams() method allows the user to set the initially guessed parameters.
		 * @param[in] ai_magnitude	Magnitude
		 * @param[in] ai_width		Width
		 * @param[in] ai_order		Order
		 * @param[in] ai_center		Center
		 */
		void setGuessParams(double ai_magnitude, double ai_width, double ai_order, double ai_center);

		/**
		 * The apply method shall launch the fitting on provided Y data sets. Overloaded method.
		 * param[in] ai_dataSize Data sets size.
		 * param[in] ai_YData Ordinate data.
		*/
		int apply(size_t ai_dataSize, const double* ai_YData);

		/**
		 * The apply method shall launch the fitting on provided X and Y data sets. Overloaded method.
		 * param[in] ai_dataSize Data sets size.
		 * param[in] ai_XData Abscissa data.
		 * param[in] ai_YData Ordinate data.
		*/
		int apply(size_t ai_dataSize, const double* ai_XData, const double* ai_YData);

		/* The following methods are inheritated from Fitter class : 

		const std::vector<double>&	getFittedData() const;
		const std::vector<double>&	getResidualData() const;
		int							getIterationNumber() const;
		double						getEpsilon() const;
		bool						hasConverged() const;
		const std::string&			getStatus() const;
		void						setEpsilon( double ai_epsilon);
		*/

		/**
		 * The setIterationNumber method shall set the maximum iteration number which the fitting
		 * algorithm may do. Overloaded method.
		 * param[in] filename Output data filename.
		*/
		void setIterationNumber( int ai_nbIteration);

		/**
		 * getMagnitude method returns the calculated magnitude parameter of the fitted spectrum. Inline method.
		 * @return Magnitude.
		*/		
		inline double getMagnitude() const;
		/**
		 * getMagnitude method returns the calculated width parameter of the fitted spectrum. Inline method.
		 * @return Width.
		*/		
		inline double getWidth() const;
		/**
		 * getMagnitude method returns the calculated order parameter of the fitted spectrum. Inline method.
		 * @return Order.
		*/		
		inline double getOrder() const;
		/**
		 * getMagnitude method returns the calculated center parameter of the fitted spectrum. Inline method.
		 * @return Center.
		*/		
		inline double getCenter() const;

		/**
		 * getMagnitude method returns the calculated center parameter of the fitted spectrum. This method do not give 
		 * accurate value for super gaussian fitted data. Inline method.
		 * @return Center.
		*/	
		double getSigma() const;
		/**
		 * getVariance method returns the calculated variance of the fitted spectrum. This method do not give 
		 * accurate value for super gaussian fitted data. Inline method.
		 * @return Variance.
		*/	
		double getVariance() const;
		/**
		 * getFWHM method returns the calculated FWHM of the fitted spectrum. Inline method.
		 * @return FWHM.
		*/	
		double getFWHM() const;
		/**
		 * get1_e2 method returns the calculated 1/e^2 of the fitted spectrum. Inline method.
		 * @return 1/e^2.
		*/	
		double get1_e2() const;
		/**
		 * getChi2 method returns the calculated chi2 of the fitted spectrum. Inline method.
		 * @return chi2.
		*/	
		double getChi2() const;		
		
		/**
		 * The dumpCalculatedValues method shall dump in a file all calculated scalar data in a 
		 * CSV compatible format. Abstract method.
		 * param[in] filename Output data filename.
		*/
		void dumpCalculatedValues(const std::string& filename);
		/**
		 * The dumpVectorData method shall dump in a file all calculated spectrum data in a CSV 
		 * compatible format. Abstract method.
		 * param[in] filename Output data filename.
		*/
		void dumpVectorData(const std::string& filename);
	private:
		int			m_paramNbr;
		double*		m_paramVal;

	private:
		// Do not allow the copy of this object
		SuperGaussianFit& operator= (const SuperGaussianFit& ) {return *this;/* Do nothing */}
		SuperGaussianFit(const SuperGaussianFit&) {/* Do nothing*/}

	};

	double SuperGaussianFit::getMagnitude() const
	{
		return m_paramVal[0];
	}
	double SuperGaussianFit::getWidth() const
	{
		return m_paramVal[1];
	}
	double SuperGaussianFit::getOrder() const
	{
		return m_paramVal[2];
	}
	double SuperGaussianFit::getCenter() const
	{
		return m_paramVal[3];
	}

}

#endif /* !SuperGaussianFit_H */