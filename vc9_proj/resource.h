//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CSCI.rc
//

#define CSCI_VERSION_NUMBER		0,1,0,14
#define CSCI_VERSION_STRING		"0, 1, 0, 14\0"
#define CSCI_VERSION_DATE		"08/08/2014\0"
#define CSCI_COMPANY			"THALES-OPTRONIQUE S.A. France\0"
#define CSCI_PRODUCT_FAMILY		"ELI-NP CSCI COMMON 62986082AA\0"
#define CSCI_FILE_DESCRIPTION   "Fitter COMMON Library \0"
#define CSCI_ORIGINAL_FILENAME  "Fitter.dll\0"
