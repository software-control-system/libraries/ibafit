from conan import ConanFile

class ibafitRecipe(ConanFile):
    name = "ibafit"
    version = "1.0.0"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"

    license = "GPL-2"
    author = "Falilou Thiam"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/plcserverproxy"
    description = "IBA fitting library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**"
